import React from 'react';
import './App.css'; 
import { Router } from '@reach/router';
import Demo1 from './Demo1';
import Demo2 from './Demo2';
import Demo3 from './Demo3';

function App() {
  return (
    <div>
      <Router>
        <Demo1 path="/" />
        <Demo2 path="/demo2" />
        <Demo3 path="/demo3" />
      </Router>
    </div>
  );
}

export default App;
