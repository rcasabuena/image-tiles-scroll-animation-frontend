import LocomotiveScroll from 'locomotive-scroll';

const LocomotiveScrollEffect = el => {
    const scroll = new LocomotiveScroll({
      el: el,
      smooth: true
    });
    return scroll;
}

export default LocomotiveScrollEffect