import React from 'react'
import { Link } from '@reach/router'

function NavLink(props) {
  return (
    <Link
      {...props}
      getProps={({ isCurrent }) => {
        return isCurrent ? { className: "frame__demo frame__demo--current" } : { className: "frame__demo" }
      }}
    />
  )
}

export default NavLink
