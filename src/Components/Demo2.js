import React, { useEffect, useRef, useState } from 'react'
import LocomotiveScrollEffect from './LocomotiveScrollEffect'
import { preloadImages, preloadFonts } from '../utils'
import Header from './Header';
import Footer from './Footer';
import BackToTop from './BackToTop';

function Demo2() {
  
  const [scroll, setScroll] = useState(null)
  const headerContainer = useRef(null);
  const scrollContainer = useRef(null)
  useEffect(() => {
    document.title = 'Image Tiles Scroll Animation | Demo 2';
    document.body.className = 'demo-2 loading';
    
    Promise.all([preloadImages('.tiles__line-img'), preloadFonts('rmd7deq')]).then(() => {
      setTimeout(() => document.body.classList.remove('loading'), 2000)
      const locomotiveScroll = LocomotiveScrollEffect(scrollContainer.current);
      setScroll(locomotiveScroll)
    });

    return () => { 
      document.body.className = "";
      document.title = "";   
    };
  }, []);

  return (
    <div ref={scrollContainer} >
			<section ref={headerContainer} id="header" className="content content--fixed">
				<Header />
			</section>
			<section className="tiles tiles--columns-rotated tiles--darker" id="grid">
				<div className="tiles__wrap">
					<div className="tiles__line" data-scroll data-scroll-speed="1" data-scroll-target="#grid">
						<div className="tiles__line-img" style={{backgroundImage:`url(img/demo2/1.jpg)`}}></div>
						<div className="tiles__line-img" style={{backgroundImage:`url(img/demo2/2.jpg)`}}></div>
						<div className="tiles__line-img" style={{backgroundImage:`url(img/demo2/3.jpg)`}}></div>
						<div className="tiles__line-img" style={{backgroundImage:`url(img/demo2/4.jpg)`}}></div>
						<div className="tiles__line-img" style={{backgroundImage:`url(img/demo2/5.jpg)`}}></div>
					</div>
					<div className="tiles__line" data-scroll data-scroll-speed="-1" data-scroll-target="#grid">
						<div className="tiles__line-img" style={{backgroundImage:`url(img/demo2/5.jpg)`}}></div>
						<div className="tiles__line-img" style={{backgroundImage:`url(img/demo2/6.jpg)`}}></div>
						<div className="tiles__line-img" style={{backgroundImage:`url(img/demo2/7.jpg)`}}></div>
						<div className="tiles__line-img" style={{backgroundImage:`url(img/demo2/8.jpg)`}}></div>
						<div className="tiles__line-img" style={{backgroundImage:`url(img/demo2/9.jpg)`}}></div>
					</div>
					<div className="tiles__line" data-scroll data-scroll-speed="1" data-scroll-target="#grid">
						<div className="tiles__line-img" style={{backgroundImage:`url(img/demo2/9.jpg)`}}></div>
						<div className="tiles__line-img" style={{backgroundImage:`url(img/demo2/10.jpg)`}}></div>
						<div className="tiles__line-img" style={{backgroundImage:`url(img/demo2/11.jpg)`}}></div>
						<div className="tiles__line-img" style={{backgroundImage:`url(img/demo2/12.jpg)`}}></div>
						<div className="tiles__line-img" style={{backgroundImage:`url(img/demo2/13.jpg)`}}></div>
					</div>
					<div className="tiles__line" data-scroll data-scroll-speed="-1" data-scroll-target="#grid">
						<div className="tiles__line-img" style={{backgroundImage:`url(img/demo2/13.jpg)`}}></div>
						<div className="tiles__line-img" style={{backgroundImage:`url(img/demo2/14.jpg)`}}></div>
						<div className="tiles__line-img" style={{backgroundImage:`url(img/demo2/15.jpg)`}}></div>
						<div className="tiles__line-img" style={{backgroundImage:`url(img/demo2/16.jpg)`}}></div>
						<div className="tiles__line-img" style={{backgroundImage:`url(img/demo2/17.jpg)`}}></div>
					</div>
					<div className="tiles__line" data-scroll data-scroll-speed="1" data-scroll-target="#grid">
						<div className="tiles__line-img" style={{backgroundImage:`url(img/demo2/17.jpg)`}}></div>
						<div className="tiles__line-img" style={{backgroundImage:`url(img/demo2/18.jpg)`}}></div>
						<div className="tiles__line-img" style={{backgroundImage:`url(img/demo2/19.jpg)`}}></div>
						<div className="tiles__line-img" style={{backgroundImage:`url(img/demo2/20.jpg)`}}></div>
						<div className="tiles__line-img" style={{backgroundImage:`url(img/demo2/1.jpg)`}}></div>
					</div>
				</div>
				<h2 className="tiles__title">Constrasts</h2>
			</section>
			<section className="content">
				<p className="content__text content__text--centered" data-scroll data-scroll-speed="4">Psychedelics are illegal not because a loving government is concerned that you may jump out of a third story window. Psychedelics are illegal because they dissolve opinion structures and culturally laid down models of behaviour and information processing. They open you up to the possibility that everything you know is wrong.</p>
			</section>
			<section className="content content--feature">
				<p className="content__breakout content__breakout--big" data-scroll data-scroll-speed="1"  data-scroll-direction="horizontal">pursuit of happiness</p>
				<p className="content__breakout content__breakout--medium" data-scroll data-scroll-speed="2"  data-scroll-direction="horizontal">the right to experiment with your own consciousnes</p>
			</section>
			<section className="tiles tiles--small" id="grid2">
				<div className="tiles__wrap">
					<div className="tiles__line">
						<div className="tiles__line-img" style={{backgroundImage:`url(img/demo1/10.jpg)`}} data-scroll data-scroll-speed="1" data-scroll-target="#grid2"></div>
						<div className="tiles__line-img" style={{backgroundImage:`url(img/demo1/20.jpg)`}} data-scroll data-scroll-speed="-1" data-scroll-target="#grid2"></div>
						<div className="tiles__line-img" style={{backgroundImage:`url(img/demo1/3.jpg)`}} data-scroll data-scroll-speed="1" data-scroll-target="#grid2"></div>
						<div className="tiles__line-img" style={{backgroundImage:`url(img/demo1/4.jpg)`}} data-scroll data-scroll-speed="-1" data-scroll-target="#grid2"></div>
						<div className="tiles__line-img" style={{backgroundImage:`url(img/demo1/19.jpg)`}} data-scroll data-scroll-speed="1" data-scroll-target="#grid2"></div>
						<div className="tiles__line-img" style={{backgroundImage:`url(img/demo1/11.jpg)`}} data-scroll data-scroll-speed="-1" data-scroll-target="#grid2"></div>
						<div className="tiles__line-img" style={{backgroundImage:`url(img/demo1/28.jpg)`}} data-scroll data-scroll-speed="1" data-scroll-target="#grid2"></div>
						<div className="tiles__line-img" style={{backgroundImage:`url(img/demo1/26.jpg)`}} data-scroll data-scroll-speed="-1" data-scroll-target="#grid2"></div>
					</div>
				</div>
			</section>
			<Footer>
        <BackToTop scroll={scroll} headerEl={headerContainer.current} />
      </Footer>
		</div>
  )
}

export default Demo2
