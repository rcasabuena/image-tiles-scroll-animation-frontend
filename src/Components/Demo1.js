import React, { useEffect, useRef, useState } from 'react'
import LocomotiveScrollEffect from './LocomotiveScrollEffect'
import { preloadImages, preloadFonts } from '../utils'
import Header from './Header';
import Footer from './Footer';
import BackToTop from './BackToTop'

function Demo1() {

  // const imgGrid2 = {
  //   1: {
  //     1: null, 
  //     2: null, 
  //     3: null, 
  //     4: 'img/demo1/4.jpg', 
  //     5: 'img/demo1/5.jpg', 
  //     6: 'img/demo1/5.jpg'
  //   },
  //   2: {
  //     1: null, 
  //     2: 'img/demo1/8.jpg', 
  //     3: 'img/demo1/9.jpg', 
  //     4: 'img/demo1/10.jpg', 
  //     5: 'img/demo1/11.jpg', 
  //     6: 'img/demo1/12.jpg'
  //   },
  //   3:{
  //     1: 'img/demo1/13.jpg', 
  //     2: 'img/demo1/14.jpg', 
  //     3: 'img/demo1/15.jpg', 
  //     4: 'img/demo1/16.jpg', 
  //     5: 'img/demo1/17.jpg', 
  //     6: 'img/demo1/18.jpg'
  //   },
  //   4: {
  //     1: 'img/demo1/19.jpg', 
  //     2: 'img/demo1/20.jpg', 
  //     3: 'img/demo1/21.jpg', 
  //     4: 'img/demo1/22.jpg', 
  //     5: 'img/demo1/23.jpg', 
  //     6: null
  //   },
  //   5: {
  //     1: null, 
  //     2: 'img/demo1/26.jpg', 
  //     3: 'img/demo1/27.jpg', 
  //     4: 'img/demo1/28.jpg', 
  //     5: null, 
  //     6: null
  //   }
  // }
  
  const [scroll, setScroll] = useState(null)
  const scrollContainer = useRef(null);
  const headerContainer = useRef(null);
  useEffect(() => {

    document.title = 'Image Tiles Scroll Animation | Demo 1';
    document.body.className = 'demo-1 loading';

    Promise.all([preloadImages('.tiles__line-img'), preloadFonts('rmd7deq')]).then(() => {
      setTimeout(() => document.body.classList.remove('loading'), 2000)
      const locomotiveScroll = LocomotiveScrollEffect(scrollContainer.current);
      setScroll(locomotiveScroll)
    });

    return () => { 
      document.body.className = "";
      document.title = "";   
    };
  }, []);

  return (
    <div ref={scrollContainer} id="main">
      <section ref={headerContainer} id="header" className="content content--numbered">
        <Header />
        <p className="content__pretitle">Unseeing the seen</p>
        <h1 className="content__title" data-scroll data-scroll-speed="2">decondition yourself</h1>
      </section>
      <section className="tiles tiles--rotated" id="grid2">
        <div className="tiles__wrap">
          <div className="tiles__line" data-scroll data-scroll-speed="1" data-scroll-target="#grid2" data-scroll-direction="horizontal">
            <div className="tiles__line-img"></div>
            <div className="tiles__line-img"></div>
            <div className="tiles__line-img"></div>
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo1/4.jpg)`}}></div>
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo1/5.jpg)`}}></div>
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo1/6.jpg)`}}></div>
          </div>
          <div className="tiles__line" data-scroll data-scroll-speed="-1" data-scroll-target="#grid2" data-scroll-direction="horizontal">
            <div className="tiles__line-img"></div>
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo1/8.jpg)`}}></div>
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo1/9.jpg)`}}></div>
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo1/10.jpg)`}}></div>
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo1/11.jpg)`}}></div>
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo1/12.jpg)`}}></div>
          </div>
          <div className="tiles__line" data-scroll data-scroll-speed="1" data-scroll-target="#grid2" data-scroll-direction="horizontal">
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo1/13.jpg)`}}></div>
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo1/14.jpg)`}}></div>
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo1/15.jpg)`}}></div>
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo1/16.jpg)`}}></div>
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo1/17.jpg)`}}></div>
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo1/18.jpg)`}}></div>
          </div>
          <div className="tiles__line" data-scroll data-scroll-speed="-1" data-scroll-target="#grid2" data-scroll-direction="horizontal">
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo1/19.jpg)`}}></div>
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo1/20.jpg)`}}></div>
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo1/21.jpg)`}}></div>
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo1/22.jpg)`}}></div>
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo1/23.jpg)`}}></div>
            <div className="tiles__line-img"></div>
          </div>
          <div className="tiles__line" data-scroll data-scroll-speed="1" data-scroll-target="#grid2" data-scroll-direction="horizontal">
            <div className="tiles__line-img"></div>
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo1/26.jpg)`}}></div>
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo1/27.jpg)`}}></div>
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo1/28.jpg)`}}></div>
            <div className="tiles__line-img"></div>
            <div className="tiles__line-img"></div>
          </div>
        </div>
      </section>
      <section className="content content--numbered">
        <p className="content__text" data-scroll data-scroll-speed="2">The human world is exploding at the seams. Human creativity and the implementation of human inventions and technology is now at an accelerated fever pitch like nothing ever before seen in the history of the world. Well, where is it leading, and how does one integrate this stuff into one’s own life? What does it mean about the experience of being human? I have proposed the existence of an invisible, permeating <em>Something</em> that is throughout all being, all time, all space, all bodies, all thought, which I call <em>Novelty</em>; and the interesting thing about novelty is that it’s increasing constantly. </p>
      </section>
      <section className="tiles" id="grid">
        <div className="tiles__wrap">
          <div className="tiles__line" data-scroll data-scroll-speed="1" data-scroll-target="#grid" data-scroll-direction="horizontal">
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo1/30.jpg)`}}></div>
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo1/29.jpg)`}}></div>
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo1/28.jpg)`}}></div>
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo1/27.jpg)`}}></div>
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo1/26.jpg)`}}></div>
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo1/25.jpg)`}}></div>
          </div>
          <div className="tiles__line" data-scroll data-scroll-speed="2" data-scroll-target="#grid" data-scroll-direction="horizontal">
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo1/24.jpg)`}}></div>
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo1/23.jpg)`}}></div>
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo1/22.jpg)`}}></div>
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo1/19.jpg)`}}></div>
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo1/18.jpg)`}}></div>
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo1/17.jpg)`}}></div>
          </div>
          <div className="tiles__line" data-scroll data-scroll-speed="1" data-scroll-target="#grid" data-scroll-direction="horizontal">
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo1/16.jpg)`}}></div>
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo1/15.jpg)`}}></div>
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo1/14.jpg)`}}></div>
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo1/13.jpg)`}}></div>
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo1/12.jpg)`}}></div>
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo1/11.jpg)`}}></div>
          </div>
        </div>
      </section>
      <section className="content content--feature">
        <p className="content__breakout content__breakout--big" data-scroll data-scroll-speed="3" data-scroll-direction="horizontal">endless acceleration toward infinity</p>
        <p className="content__breakout content__breakout--medium" data-scroll data-scroll-speed="-1" data-scroll-direction="horizontal">the greatest barrier to your enlightenment</p>
      </section>
      <section className="content content--numbered">
        <p className="content__text" data-scroll data-scroll-speed="2">The conservation of novelty is simply that, over time, the universe has become more complicated. New levels of complexity become the foundations for yet deeper levels of complexity. And this phenomenon of the production and conservation of what I call novelty is not something which goes on only in the biological domain or only in the cultural domain or only in the domain of physics. It is a trans-categorical impulse in reality, meaning: it’s everywhere. <em>Everywhere!</em></p>
      </section>
      <section className="tiles tiles--columns" id="grid3">
        <div className="tiles__wrap">
          <div className="tiles__line" data-scroll data-scroll-speed="1" data-scroll-target="#grid3" data-scroll-direction="vertical">
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo2/20.jpg)`}}></div>
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo2/19.jpg)`}}></div>
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo2/18.jpg)`}}></div>
          </div>
          <div className="tiles__line" data-scroll data-scroll-speed="-1" data-scroll-target="#grid3" data-scroll-direction="vertical">
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo2/16.jpg)`}}></div>
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo2/3.jpg)`}}></div>
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo2/14.jpg)`}}></div>
          </div>
          <div className="tiles__line" data-scroll data-scroll-speed="1" data-scroll-target="#grid3" data-scroll-direction="vertical">
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo2/12.jpg)`}}></div>
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo2/11.jpg)`}}></div>
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo2/10.jpg)`}}></div>
          </div>
          <div className="tiles__line" data-scroll data-scroll-speed="-1" data-scroll-target="#grid3" data-scroll-direction="vertical">
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo2/8.jpg)`}}></div>
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo2/7.jpg)`}}></div>
            <div className="tiles__line-img" style={{backgroundImage:`url(img/demo2/6.jpg)`}}></div>
          </div>
        </div>
      </section>
      <Footer>
        <BackToTop scroll={scroll} headerEl={headerContainer.current} />
      </Footer>
    </div>
  )
}

export default Demo1
