import React from 'react'
import NavLink from './NavLink'

function NavLinks() {
  return (
    <div className="frame__demos">
      <NavLink to="/">Demo 1</NavLink>
      <NavLink to="/demo2">Demo 2</NavLink>
      <NavLink to="/demo3">Demo 3</NavLink>
    </div>
  )
}

export default NavLinks
