import React from 'react'
import NavLinks from './NavLinks'

function Header() {
  return (
    <div className="frame">
      <div className="frame__title-wrap">
        <h1 className="frame__title">Image Tiles Scroll Animation with <a href="https://locomotivemtl.github.io/locomotive-scroll/" target="_blank" rel="noopener noreferrer">Locomotive Scroll</a></h1>
      </div>
      <div className="frame__links">
        <a href="https://bitbucket.org/rcasabuena/image-tiles-scroll-animation-frontend" target="_blank" rel="noopener noreferrer" >BitBucket</a>
      </div>
      <NavLinks />
    </div>
  )
}

export default Header
