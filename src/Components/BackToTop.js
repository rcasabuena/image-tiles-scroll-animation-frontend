import React, { useEffect, useRef } from 'react'
import LocomotiveScroll from 'locomotive-scroll';

function BackToTop({ scroll, headerEl }) {
  const backToTop = useRef(null)
  useEffect(() => {
    if (scroll instanceof LocomotiveScroll) {
      backToTop.current.addEventListener('click', () => scroll.scrollTo(headerEl))
    }
  }, [scroll, headerEl])

  return (
    <p ref={backToTop} className="backtop" data-scroll data-scroll-speed="4">Back to the top</p>
  )
}

export default BackToTop
