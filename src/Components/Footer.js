import React from 'react'
import NavLinks from './NavLinks'

function Footer({ children }) {
  return (
    <section className="content">
      {children}
      <div className="frame frame--footer">
        <a href="https://bitbucket.org/rcasabuena/" className="frame__author" target="_blank" rel="noopener noreferrer">Ritchie Casabuena</a>
        <NavLinks />
      </div>
    </section>
  )
}

export default Footer
